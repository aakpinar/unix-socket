# unix-socket

## Project status
- branch v1 contains scripts to send strings over socket between client/server processes
- branch v2 builds on this by serializing the strings (or a more complex data structure) via Google Buffer Protocol
